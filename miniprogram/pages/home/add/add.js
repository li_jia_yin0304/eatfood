// pages/add/add.js
Page({
  data: {
    // 菜品名字
    caidan: '',
    // 菜品价格
    price: '',
    // 菜品图片链接
    imgList: [],
    // 上传菜品后的cloud链接
    fileIDs: [],
    // 单选多选默认false选择
    checked: false,
    // 多选内容
    campusList: [{
      campusId: 1,
      name: '一饭',
      // value:"一饭"
    }, {
      campusId: 2,
      name: '二饭',
      // value:"二饭"
    }, {
      campusId: 3,
      name: '三饭',
      // value:"三饭"
    }, {
      campusId: 4,
      name: '四饭',
      // value:"四饭"
    }],
    // 当前选中的食堂数字（0为未选择）
    currentCampus: 1,
  },

  // 多选
  chooseCampus: function (options) {
    var id = options.currentTarget.dataset.id;
    //设置当前样式
    this.setData({
      'currentCampus': id
    })
    console.log(options)
  },

  // 添加菜品
  save() {
    let FoodArr = [this.data.caidan, this.data.price, this.data.currentCampus, this.data.imgList]
    console.log(FoodArr)
  },

  //清空添加菜品
  clear() {
    this.setData({
      caidan: '',
      FoodMoney: '',
      currentCampus: 0,
      checked: false,
      imgList: ''
    })
  },

  // 清空数据
  clears() {
    wx.showModal({
      title: "清空",
      content: "确定清空",
      success: res => {
        if (res.confirm) {
          this.setData({
              caidan: '',
              FoodMoney: '',
              currentCampus: 0,
              checked: false,
              imgList: ''
            }),
            wx.showToast({
              title: '清空成功',
            })
        }
        console.log("清空成功：", res)

      },

    })
  },
  // wx.showModal({
  //   title:"清空",
  //   content:"确定清空吗？",
  //   success(res){
  // wx.showToast({
  //   title: '已清空',
  // })
  // }
  // })



  //价格
  FoodMoney(res) {
    this.setData({
      FoodMoney: res.detail.value
    })
  },
  //菜品
  caidan(res) {
    this.setData({
      caidan: res.detail.value
    })
  },
  //选择图片
  ChooseImage() {
    wx.chooseImage({
      count: 8 - this.data.imgList.length, //默认9,我们这里最多选择8张
      sizeType: ['compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], //从相册选择
      success: (res) => {
        console.log("选择图片成功", res)
        if (this.data.imgList.length != 0) {
          this.setData({
            cloudPath: res.tempFilePaths[0].replace(/(.*\/)*([^.])+/i, "$2"),
            imgList: res.tempFilePaths[0]
          })
        } else {
          this.setData({
            imgList: res.tempFilePaths
          })
        }
        console.log("路径", this.data.imgList)
      }
    });
  },

  //删除图片
  DeleteImg(e) {
    wx.showModal({
      title: '要删除这张照片吗？',
      content: '',
      cancelText: '取消',
      confirmText: '确定',
      success: res => {
        if (res.confirm) {
          this.data.imgList.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList: this.data.imgList
          })
        }
      }
    })
  },

  //上传数据
  publish() {
    let desc = this.data.desc
    let imgList = this.data.imgList
    if (!desc || desc.length < 6) {
      wx.showToast({
        icon: "none",
        title: '内容大于6个字'
      })
      return
    }
    if (!imgList || imgList.length < 1) {
      wx.showToast({
        icon: "none",
        title: '请选择图片'
      })
      return
    }
    wx.showLoading({
      title: '发布中...',
    })
    const promiseArr = []
    //只能一张张上传 遍历临时的图片数组
    for (let i = 0; i < this.data.imgList.length; i++) {
      let filePath = this.data.imgList[i]
      let suffix = /\.[^\.]+$/.exec(filePath)[0]; // 正则表达式，获取文件扩展名
      //在每次上传的时候，就往promiseArr里存一个promise，只有当所有的都返回结果时，才可以继续往下执行
      promiseArr.push(new Promise((reslove, reject) => {
        wx.cloud.uploadFile({
          cloudPath: res.tempFilePaths[0].replace(/(.*\/)*([^.])+/i, "$2"),
          filePath: res.tempFilePaths[0], // 文件路径
        }).then(res => {
          // get resource ID
          console.log("上传结果", res.fileID)
          this.setData({
            fileIDs: this.data.fileIDs.concat(res.fileID)
          })
          reslove()
        }).catch(error => {
          console.log("上传失败", error)
        })
      }))
    }
    //保证所有图片都上传成功
    let db = wx.cloud.database()
    Promise.all(promiseArr).then(res => {
      db.collection('timeline').add({
        data: {
          fileIDs: this.data.fileIDs,
          date: app.getNowFormatDate(),
          createTime: db.serverDate(),
          desc: this.data.desc,
          images: this.data.imgList
        },
        success: res => {
          wx.hideLoading()
          wx.showToast({
            title: '发布成功',
          })
          console.log('发布成功', res)
          wx.navigateTo({
            url: '../pengyouquan/pengyouquan',
          })
        },
        fail: err => {
          wx.hideLoading()
          wx.showToast({
            icon: 'none',
            title: '网络不给力....'
          })
          console.error('发布失败', err)
        }
      })
    })
  },

  // 选择图片
  chooseImg: function () {
    let that = this
    wx.chooseImage({
      count: 1000,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        const imgurl = res.tempFilePaths
        that.setData({
          imgurl
        })
      }
    })
  },

  //添加上传云开发的数据菜品
  submit: function (e) {
    //  console.log(e.detail.value)
    //更新数据
    var formdata = e.detail.value;
    console.log(this.data.campusList[this.data.currentCampus - 1].name)
    this.setData({
      "data.FoodHouse": this.data.campusList[this.data.currentCampus - 1].name,
      "data.FoodMoney": formdata.FoodMoney,
      "data.FoodName": formdata.FoodName,
      "data.Foodtime": formdata.Foodtime,
      "data.ImgPath": formdata.filePath
    })
    console.log("更新data", e)
    //添加数据
   var getdata = this.data;
   wx.cloud.callFunction({
      name:'postfood',
      data: { 
        FoodHouse: getdata.campusList[this.data.currentCampus - 1].name,
        FoodMoney: getdata.data.FoodMoney,
        FoodName: getdata.data.FoodName,
        Foodtime: getdata.data.Foodtime,
        ImgPath: getdata.imgList[0]
      }
    }).then(res => {
      wx.showToast({
        title: '菜品添加成功',
      })
      console.log("添加至數據庫成功", res)
      this.clear()
    }).catch(res => {
      console.log("添加失敗", res)
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (option) {},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})