const App = getApp();
const db = wx.cloud.database()
const food = db.collection('food')
const foot = db.collection('foot')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 食物数组
    foods: [{
      FoodMoney: 'xx',
      FoodName: 'xx',
      ImgPath: '../../../images/jixiangwu.png'
    }],
    // 随机数字
    sj: 0,
    //点击次数
    dianji: 0,
    // 气泡文字
    campusList: [{
      campusId: 1,
      name: '早'
    }, {
      campusId: 2,
      name: '午'
    }, {
      campusId: 3,
      name: '晚'
    }],
    // 气泡中多选选中的campusId
    currentCampus: 1,
    // 当前的一个足迹
    footsave: [{}],
    // 所有点击过的足迹
    foots: []
  },

  // 点击添加菜品
  addfood: function () {
    wx.navigateTo({
      url: '../addfood/index',
      success: function (res) {},
      fail: function (res) {},
      complete: function (res) {},
    })
  },

  // 点击随机的图片
  change() {
    let dianji = this.data.dianji + 1
    this.setData({
      dianji: dianji
    })
    if (this.data.foods.length > 1) {
      this.sjnum()
    } else {
      // this.getfoods()
    }
  },

  // 随机数字
  sjnum() {
    let sj = parseInt(Math.random() * (this.data.foods.length), 10)
    let footsave = sj
    console.log(footsave);
    let date = this.data.foods[sj]
    date.date = new Date()
    let foots = this.data.foots
    foots.push(date)
    this.setData({
      sj: sj,
      footsave: [this.data.foods[sj]],
      foots: foots
    })
  },

  //单选
  chooseCampus: function (options) {
    var id = options.currentTarget.dataset.id;
    //设置当前样式
    this.setData({
      'currentCampus': id
    })
    if (this.data.currentCampus == 1) {
      console.log("没有效果，我还没弄hhhh")
    } else if (this.data.currentCampus == 2) {
      console.log("没有效果，我还没弄hhhh")
    } else {
      console.log("没有效果，我还没弄hhhh")
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    if (this.data.foods.length <= 1) {
      wx.cloud.callFunction({
        // 云函数名称
        name: 'food',
        // 传给云函数的参数
        data: {
          a: 1, // 传给云函数的参数
          b: 2, // 传给云函数的参数
        },
        success: res => {
          var that = this;
          console.log(res.result.data)
          that.setData({
            foods: res.result.data
          })
        },
      })
    } else {
      console.log("已经加载完毕")
    }
    // 每隔一秒钟调用一次最新的本地存储，怕出现无法及时更新数据数组的情况
    setInterval(that.getaddfoods, 1000)
  },

  // 调用addfood传过来的本地存储
  getaddfoods: function () {
    wx.getStorage({
      key: 'addfoods',
    }).then(res => {
      if (res.data.length >= 0) {
        this.setData({
          foods: res.data
        })
      }
    }).catch(e => {})
    // console.log("b")
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {


  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    // 页面隐藏时把生成的数据保存下来
    wx.setStorage({
      data: this.data.foods,
      key: 'foods'
    }).then(() => {})

    wx.setStorage({
      data: this.data.foots,
      key: 'foots'
    }).then(() => {})
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '发际线与我作队',
      desc: '小组作品',
      path: '/pages/home/index/index'
    }
  }
})