Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 缓存里面的食物数据
    foods: [],
    // 输入框输入的字符
    SearchVal: '',
    //是否长按
    longtap: 0,
    //长按删除的数组
    deleteArr: [],
    //存储长按后点击的数字
    cindex: null
  },

  // 点击猫头鹰跳转add
  add() {
    wx.navigateTo({
      url: '../add/add',
    })
  },

  // 输入框输入事件
  SearchVal: function (res) {
    this.setData({
      SearchVal: res.detail.value
    })
  },

  // 回车添加菜品
  addfood: function (res) {
    let foods = this.data.foods
    foods.unshift({
      FoodName: this.data.SearchVal
    })
    this.setData({
      foods: foods,
      SearchVal: ''
    })
    wx.showToast({
      title: '添加成功',
    })
    this.getaddfoods()
  },

  // 将当前存储的foods数据保存到本地
  getaddfoods: function () {
    wx.setStorage({
      data: this.data.foods,
      key: 'addfoods',
    })
  },

  //长按删除
  checkdelete: function (e) {
    console.log(e)
    this.longtap = 1
  },

  //删除
  delete: function (res) {
    if (this.longtap != 1) {
      var that = this
      let index = res.currentTarget.dataset.id
      let FoodName = that.data.foods[res.currentTarget.dataset.id].FoodName
      let foods = that.data.foods
      wx.showModal({
        title: '删除',
        content: '删除' + FoodName + '菜品？',
        success(res) {
          if (res.confirm) {
            let delfoods = foods
            delfoods.splice(index, 1)
            that.setData({
              foods: delfoods
            })
            that.getaddfoods()
            wx.showToast({
              title: '删除成功',
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
      console.log(res)
    } else {
      this.data.cindex = res.currentTarget.dataset.id
      this.data.deleteArr.push(this.data.cindex)
      console.log("s")
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.getStorage({
      key: 'foods',
    }).then(res => {
      this.setData({
        foods: res.data
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }

})