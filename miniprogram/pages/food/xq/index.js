let app = getApp()
const db = wx.cloud.database()
const star = db.collection('star')
Page({
  data: {
    // 页面传递食物id
    key: '',
    // 当前食物的数组
    food: [],
    // 当前用户默认是否收藏
    stared: false
  },

  // 点击收藏
  async isStared() {
    // 若没有登录
    if (!app.globalData.user) {
      let ress = await wx.getUserProfile({
        desc: '用于完善用户信息',
      })
      console.log(ress)
      let {
        nickName,
        avatarUrl
      } = ress.userInfo
      app.globalData.user = ress.userInfo
      this.setData({
        nickName,
        avatarUrl
      })
      let fooduser = ress.userInfo
      console.log(fooduser)
      let users = await wx.cloud.callFunction({
        name: 'FoodGetUsers',
        data: {
          fooduser: fooduser
        }
      })
      console.log(users)
      let stared = !this.data.stared
      this.setData({
        stared
      })

      let res = await wx.cloud.callFunction({
        name: "PostStar",
        data: {
          food: this.data.food,
          stared: this.data.stared,
          FoodId: Number(this.data.key)
        }
      })
      if (this.data.stared) {
        wx.showToast({
          title: '收藏成功',
        })
      } else {
        wx.showToast({
          title: '取消成功',
        })
      }
    } else {
      let stared = !this.data.stared
      this.setData({
        stared
      })
      let res = await wx.cloud.callFunction({
        name: "PostStar",
        data: {
          food: this.data.food,
          stared: this.data.stared,
          FoodId: Number(this.data.key)
        }
      })
      if (this.data.stared) {
        wx.showToast({
          title: '收藏成功',
        })
      } else {
        wx.showToast({
          title: '取消成功',
        })
      }
    }
  },

  // 生命周期
  async onLoad(options) {
    wx.stopPullDownRefresh() //刷新完成后停止下拉刷新动效
    var foodid = Number(options.key)
    // console.log(foodid)
    let food = await wx.cloud.callFunction({
      name: 'getFoods',
      data: {
        foodid
      }
    })
// 获取评论
    let PL = await wx.cloud.callFunction({
      name: 'getFoodPL',
      data: {
        foodid,
        ziji: false
      }
    })
    // console.log(PL)
    // console.log(foodid)
    // 是否收藏过
    let star = await wx.cloud.callFunction({
      name: 'GetStar',
      data: {
        FoodId: foodid
      },
    })
    // console.log(star)
    this.setData({
      food: food.result.cvoted.list[0],
      key: options.key,
      PL: PL.result.getPL.list,
    })
    if (star.result.Star.list.length > 0) {
      this.setData({
        stared: star.result.Star.list[0].stared
      })
    }
    // console.log(this.data.food.hasOwnProperty('ImgPath'))
  },

  // 下拉刷新
  onPullDownRefresh: function () {
    let key = this.data.key
    this.onLoad({
      key: key
    }); //重新加载onLoad()
  },

  // 点击去评论的猫头鹰
  async pl() {
    console.log(!app.globalData.user)
    if (!app.globalData.user) {
      let res = await wx.getUserProfile({
        desc: '用于完善用户信息',
      })
      console.log(res)
      let {
        nickName,
        avatarUrl
      } = res.userInfo
      app.globalData.user = res.userInfo
      this.setData({
        nickName,
        avatarUrl
      })
      let fooduser = res.userInfo
      console.log(fooduser)
      let users = await wx.cloud.callFunction({
        name: 'FoodGetUsers',
        data: {
          fooduser: fooduser
        }
      })
      console.log(users)
      wx.navigateTo({
        url: '../pl/index?key=' + this.data.key,
      })

    } else {
      wx.navigateTo({
        url: '../pl/index?key=' + this.data.key,
      })
    }
  },

})