// pages/food/index.js
const App = getApp();
const db = wx.cloud.database()
const food = db.collection('food')
Page({


  /**
   * 页面的初始数据
   */
  data: {
    // 一二三四饭tab
    btnarr: ['一饭', '二饭', '三饭', '四饭'],
    // 所有食物
    food: [],
    searchVal: "",
  },

  // 更换一二三四饭tab
  changeTabs(e) {
    // console.log(e)
  },

  // 查看详情
  xiangqing: function (res) {
    let key = res.currentTarget.dataset.foodId
    wx.navigateTo({
      url: '../xq/index?key=' + key,
    })
  },

  // 搜索输入触发
  input(even) {
    console.log(even)
    this.setData({
      searchVal: even.detail.value
    })
  },

  // 点击搜索图标
  search: function () {
    food.where({
      FoodId: db.RegExp({
        regexp: this.data.searchVal
      })
    }).get().then(res => {
      console.log(res)
      this.setData({
        food: res.data
      })
    })
  },


  onLoad: function () {
    if (this.data.searchVal != ' ') {
      //console.log(searchVal)
      this.setData({
        searchVal: this.data.searchVal
      })
      this.search();
    } else {
      console.log("为空")
    }
    // 如果没有食物则去云函数获取食物
    if (this.data.food.length <= 0) {
      wx.cloud.callFunction({
        // 云函数名称
        name: 'food',
        // 传给云函数的参数
        data: {
          a: 1, // 传给云函数的参数
          b: 2, // 传给云函数的参数
        },
        success: res => {
          var that = this;
          console.log(res.result.data)
          that.setData({
            food: res.result.data
          })
        },
      })
    }
  },

  onShow: function () {
    wx.setStorage({
      data: this.data.food,
      key: 'food',
    }).then(res => {

    })
  }
})