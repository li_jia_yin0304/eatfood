// miniprogram/pages/food/pl/index.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 页面传递的FoodId
    key: '',
    // 当前食物的数组
    food: [],
    // 默认评价多少颗星
    score: 5
  },

  // 输入触发，保存评论数据
  tj(res) {
    console.log(res)
    this.setData({
      pl: res.detail.value
    })
  },

  // 选择星星
  changeScore(e) {
    this.setData({
      score: e.detail.score
    })
  },

  // 点击提交
  touchtj() {
    var plcontent = this.data.pl
    var FoodId = this.data.food.FoodId
    var score = this.data.score
    var date = new Date()
    wx.cloud.callFunction({
      name: 'PostFoodPL',
      data: {
        plcontent,
        FoodId,
        score,
        date
      }
    })
    wx.showToast({
      title: '评论成功',
    })
    wx.reLaunch({
      url: '../xq/index?key=' + this.data.key,
    })
    // wx.redirectTo({
    //   url: '../xq/index?key='+this.data.key,
    // })
    // wx.navigateBack({
    //   delta: 1,
    // })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    var foodid = Number(options.key)
    let food = await wx.cloud.callFunction({
      name: 'getFoods',
      data: {
        foodid
      }
    })
    this.setData({
      food: food.result.cvoted.list[0],
      key: options.key
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function (e) {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log(e)
    wx.startPullDownRefresh()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})