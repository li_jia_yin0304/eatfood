const app = getApp();
const db = wx.cloud.database()
const star = db.collection('star')
Page({
    data:{
        navbarActiveIndex:0,
        navbarTitle:[
            "我的足迹",
            "我的收藏"
        ],
        fooname:[{
            fooname:'',
            price:'',
            imgpath:''
        }],
        foot:[]
    },
    delfoot(){
        var that = this
        wx.showModal({
            title: '提示',
            content: '是否清空足迹',
            success (res) {
              if (res.confirm) {
                wx.removeStorage({
                    key: 'foots',
                    success (res) {
                       wx.redirectTo({
                         url: './foot',
                       })
                    }
                  })
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
    },
    taiozhuan(){
wx.switchTab({
  url: '../../home/index/index',
})
    },
    // 输入框的值改变 就会触发的事件
    handleInput(e){
        const {value} = e.detail
        if(!value.trim()){
            return
        }
        console.log(e)
    },
    //点击导航栏
    onNavBarTap: function(event) {
        console.log(event)
        //获取点击的navbar的index
        let navbarTapIndex = event.currentTarget.dataset.navbarIndex
        //设置data属性中的navbarActiveIndex为当前点击的navbar
        this.setData({
            navbarActiveIndex:navbarTapIndex
        })
    },
    onBindAnimationFinish:function ({detail}) {
        // console.log(detail)
        //设置data属性中的navbarActiveIndex为当前点击的navbar
        this.setData({
            navbarActiveIndex:detail.current
        })
    },

    //收藏的食物跳转
    starfood(e){
wx.navigateTo({
  url: '/pages/food/xq/index?key='+e.currentTarget.dataset.foodid,
})
    },
    async onLoad(){
      let stared = await wx.cloud.callFunction({
        name:'GetStar',
        data: {
          isTrue:true
        },
      })
this.setData({
  fooname:stared.result.Star.list
})
        var that = this
        await   wx.getStorage({
            key: 'foots',
          }).then(res => {
            let foot = res.data.map(item =>{
                item.date = app.nowdate(item.date);
                 return item
              })
              foot.reverse()
           that.setData({
               foot:foot
           })
          }).catch(e => {
          })

         
  },
  onShow(){
    // let foot = this.data.foot.map(item =>{
    //     item.date = app.nowdate(item.date);
    //     console.log(item)
    //      return item
    //   })
      // console.log(foot)
  },
  onPullDownRefresh: function () {
    let key = this.data.key
    this.onLoad({key: key}); //重新加载onLoad()
  },
})