const db = wx.cloud.database()
const PL = db.collection('PL')
Page({
    data:{
        PL:{}
    },
    del(e){
        var that=this
        let index = e.currentTarget.dataset.index
        wx.showModal({
            title: '提示',
            content: '是否删除',
            success (res) {
              if (res.confirm) {
                PL.where({"_id":that.data.PL[index].PL_id}).remove()
                .then(that.onLoad())
                .catch(console.error)
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })

    },
    pingjia(e){
let index = e.currentTarget.dataset.pingjia
let key = this.data.PL[index].FoodId
wx.navigateTo({
  url: '../../food/pl/index?key='+key,
})

    },
    async onLoad(){
let res = await wx.cloud.callFunction({
    name:'getFoodPL',
    data:{
        ziji:true
    }
})
this.setData({
    PL:res.result.getPL.list
})
    }
})