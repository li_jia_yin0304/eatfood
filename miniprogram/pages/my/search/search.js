const app = getApp()
const db = wx.cloud.database()
const star = db.collection('star')
Page({
    data:{
        isFocus:false,
        fooname:''
    },
        onLoad(options){
            star.get().then(res=>{
                console.log(res)
                this.setData({
                    fooname:res.data
                })
            })
        },
        // 输入框的值改变 就会触发的事件
        handleInput(e){
            const {value} = e.detail
            if(!value.trim()){
                return
            }
            this.setData({
                isFocus:true
            })
            console.log(e)
        }
})