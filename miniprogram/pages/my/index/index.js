let app = getApp()
Page({
  data: {
    avatarUrl:'/images/jixiangwu.png'
  },
  async onLoad(){
    if(app.globalData.user){
    this.setData({
      avatarUrl:app.globalData.user.avatarUrl
    })
      }
  },
  async getUserProfiles(){
    if(app.globalData.user){
      wx.navigateTo({
        url: '../../my/user/user',
      })
      }else{
        let res = await wx.getUserProfile({
          desc: '用于完善用户信息',
        })
        console.log(res)
        let {nickName,avatarUrl} =res.userInfo
        app.globalData.user = res.userInfo
        this.setData({
          nickName,
          avatarUrl
        })
        let fooduser = res.userInfo
        console.log(fooduser)
        let users = await wx.cloud.callFunction({
          name:'FoodGetUsers',
          data:{
             fooduser:fooduser
          }
        })
        console.log(users)
      }
  },
  foot(){
    wx.navigateTo({
      url: '../../my/foot/foot',
    })
  },
  Feedback(){
    wx.navigateTo({
      url: '../Feedback/Feedback',
    })
  },
  evaluatio(){
    wx.navigateTo({
      url: '../pingjia/index',
    })
  },
  changeTabs(){

  }
  })