// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const db=cloud.database()
  const postfoodlinshi=db.collection('food_linshi')
  let {ImgPath} =event
  let res = await postfoodlinshi.add({
    data:{
      FoodHouse: event.FoodHouse,
      FoodMoney: event.FoodMoney,
      FoodName: event.FoodName,
      Foodtime: event.Foodtime,
      ImgPath,
    }
  })
  console.log(res)
  return {
    ...res
  }
}