// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const db = cloud.database()
  const collectionPL= db.collection('PL')
  const _openid = wxContext.OPENID
  const {plcontent,FoodId,score,date} = event
  let res = await collectionPL.add({
    data:{
      plcontent,_openid,FoodId,score,date
    }
  })
  console.log(res)
    return {
      ...res
    }
}