// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const _openid = wxContext.OPENID
const {foodid,ziji} = event
const db = cloud.database()
  const colletionfood = db.collection('food')
  if(!ziji){
    var getPL = await colletionfood
    .aggregate()
    .match({
      FoodId:foodid
      })
      .lookup({
        from:'PL',
        localField:'FoodId',
        foreignField:'FoodId',
        as:'PL'
      })
      .project({
        FoodId:"$FoodId",
          FoodHouse:"$FoodHouse",
        FoodName:"$FoodName",
        ImgPath:"$ImgPath",
        PL:"$PL"
      })
      .unwind({
        path:'$PL'
      })
      .project({    
        FoodId:"$FoodId",
        FoodHouse:"$FoodHouse",
        FoodName:"$FoodName",
        ImgPath:"$ImgPath",
        PL_id:"$PL._id",
        plcontent:"$PL.plcontent",
      _openid:"$PL._openid",
      date:"$PL.date",
      score:"$PL.score"
      })
      .lookup({
        from:'FoodUser',
        localField:'_openid',
        foreignField:'_openid',
        as:'FoodUser'
      })
      .unwind({
        path:'$FoodUser'
      })
      .project({
        FoodId:"$FoodId",
            FoodHouse:"$FoodHouse",
        FoodName:"$FoodName",
        ImgPath:"$ImgPath",
        PL_id:"$PL_id",
        score:"$score",
        plcontent:"$plcontent",
      _openid:"$_openid",
      date:"$date",
      avatarUrl:"$FoodUser.fooduser.avatarUrl",
      nickName:"$FoodUser.fooduser.nickName"
      })
      .end()
  }else{
    var getPL = await colletionfood
  .aggregate()
  .match({
    FoodId:foodid
    })
    .lookup({
      from:'PL',
      localField:'FoodId',
      foreignField:'FoodId',
      as:'PL'
    })
    .project({
      FoodId:"$FoodId",
        FoodHouse:"$FoodHouse",
      FoodName:"$FoodName",
      ImgPath:"$ImgPath",
      PL:"$PL"
    })
    .unwind({
      path:'$PL'
    })
    .project({    
      FoodId:"$FoodId",
      FoodHouse:"$FoodHouse",
      FoodName:"$FoodName",
      ImgPath:"$ImgPath",
      PL_id:"$PL._id",
      plcontent:"$PL.plcontent",
    _openid:"$PL._openid",
    date:"$PL.date",
    score:"$PL.score"
    })
    .lookup({
      from:'FoodUser',
      localField:'_openid',
      foreignField:'_openid',
      as:'FoodUser'
    })
    .unwind({
      path:'$FoodUser'
    })
    .project({
      FoodId:"$FoodId",
          FoodHouse:"$FoodHouse",
      FoodName:"$FoodName",
      ImgPath:"$ImgPath",
      PL_id:"$PL_id",
      score:"$score",
      plcontent:"$plcontent",
    _openid:"$_openid",
    date:"$date",
    avatarUrl:"$FoodUser.fooduser.avatarUrl",
    nickName:"$FoodUser.fooduser.nickName"
    })
    .match({
      "_openid": _openid
    })
    .end()
  }
  
console.log(getPL)
  return {
    getPL
  }
}